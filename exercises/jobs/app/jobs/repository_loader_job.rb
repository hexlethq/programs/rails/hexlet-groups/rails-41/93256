# frozen_string_literal: true

class RepositoryLoaderJob < ApplicationJob
  queue_as :default

  def perform(repository_id)
    repository = Repository.find(repository_id)
    repository.fetch!
    repository_data = fetch_repository_data repository.link
    repository.update(repository_data)
    repository.complete!
  rescue Octokit::NotFound, StandardError
    repository.fail!
  end

  private

  def fetch_repository_data(link)
    client = Octokit::Client.new
    repository_path = URI(link).path.split('/').last(2).join('/')
    repository_data = client.repository(repository_path)
    {
      owner_name: repository_data['owner']['login'],
      repo_name: repository_data['name'],
      description: repository_data['description'],
      default_branch: repository_data['default_branch'],
      watchers_count: repository_data['watchers_count']
    }
  end
end
