# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  setup do
    @response_data = JSON.parse(load_fixture('files/response.json'))
    stub_request(:get, @response_data['url']).to_return(status: 200, body: load_fixture('files/response.json'))
  end

  test 'should create repository' do
    post repositories_path, params: { repository: { link: @response_data['url'] } }
    repository = Repository.find_by(link: @response_data['url'])
    assert repository
    assert_equal repository[:repo_name], @response_data['name']
  end
  # END
end
