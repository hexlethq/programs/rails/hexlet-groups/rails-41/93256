# frozen_string_literal: true

# BEGIN
def load_repository_data(link)
  client = Octokit::Client.new
  repository_path = URI(link).path.split('/').last(2).join('/')
  response = client.repository(repository_path)
  repository_data = JSON.parse(response)
  {
    link: link,
    owner_name: repository_data['owner']['login'],
    repo_name: repository_data['name'],
    description: repository_data['description'],
    default_branch: repository_data['default_branch'],
    watchers_count: repository_data['watchers_count']
  }
end
# END

class Web::RepositoriesController < Web::ApplicationController
  def index
    @repositories = Repository.all
  end

  def new
    @repository = Repository.new
  end

  def show
    @repository = Repository.find params[:id]
  end

  def create
    # BEGIN
    repository_data = load_repository_data(permitted_params[:link])
    @repository = Repository.new(repository_data)
    if @repository.save
      redirect_to repository_path(@repository)
    else
      render :new, notice: t('fail')
    end
    # END
  end

  def edit
    @repository = Repository.find params[:id]
  end

  def update
    @repository = Repository.find params[:id]

    if @repository.update(permitted_params)
      redirect_to repositories_path, notice: t('success')
    else
      render :edit, notice: t('fail')
    end
  end

  def destroy
    @repository = Repository.find params[:id]

    if @repository.destroy
      redirect_to repositories_path, notice: t('success')
    else
      redirect_to repositories_path, notice: t('fail')
    end
  end

  private

  def permitted_params
    params.require(:repository).permit(:link)
  end
end
