# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  # test "the truth" do
  #   assert true
  # end
  setup do
    @task = Task.first
  end

  test 'Should get index' do
    get tasks_path
    assert_response :success
    assert_select 'li', 'Task one'
    assert_select 'li', 'Task two'
  end

  test 'Should get show' do
    get task_path(@task)
    assert_select 'h1', @task.name
    assert_select 'p', "Description: #{@task.description}"
    assert_select 'p', "Status: #{@task.status}"
  end

  test 'Should get new' do
    get new_task_path
    assert_select 'input[name=?]', 'task[name]'
    assert_select 'input[name=?]', 'task[description]'
    assert_select 'input[name=?]', 'task[status]'
    assert_select 'input[name=?]', 'task[creator]'
    assert_select 'input[name=?]', 'task[performer]'
    assert_select 'select[name="task[completed]"]', 1
  end

  test 'Should create task' do
    post tasks_path, params: {
      task: {
        name: 'Test task name',
        status: 'test status',
        creator: 'John Doe',
        completed: false
      }
    }
    assert_response :redirect
    follow_redirect!
    assert_select 'h1', 'Test task name'
  end

  test 'Should not create task' do
    post tasks_path, params: {
      task: {
        name: 'Test task name',
        status: 'test status',
        creator: 'John Doe'
      }
    }
    assert_response :success
    assert_select 'h1', 'New task'
  end

  test 'Should get edit' do
    get edit_task_path(@task)
    assert_select 'input[name="task[name]"][value=?]', @task.name
  end

  test 'Should patch task' do
    patch task_path(@task), params: {
      task: {
        status: 'Done',
        completed: true
      }
    }
    assert_response :redirect
    follow_redirect!
    assert_select 'p', 'Status: Done'
    assert_select 'p', 'Completed: true'
  end

  test 'Should not patch task' do
    patch task_path(@task), params: {
      task: {
        status: ''
      }
    }
    assert_select 'h1', 'Edit task'
  end

  test 'Should delete task' do
    assert_difference('Task.count', -1) do
      delete task_path(@task)
    end
  end
end
