# frozen_string_literal: true

# BEGIN
require 'date'

module Model
  def initialize(attrs = {})
    @attributes = {}
    self.class.scheme.each do |name, _|
      value = attrs.key?(name) ? attrs[name] : nil
      type = self.class.scheme.dig(name, :type)
      @attributes[name] = self.class.set_type(value, type)
    end
  end

  module ClassMethods
    attr_accessor :scheme

    def attribute(name, params)
      @scheme ||= {}
      @scheme[name] = params
      define_method name do
        @attributes[name]
      end

      define_method "#{name}=" do |value|
        type = self.class.scheme.dig(name, :type)
        @attributes[name] = self.class.set_type(value, type)
      end
    end

    def set_type(value, type)
      return value if value.nil?

      case type
      when :string
        String value
      when :integer
        Integer value
      when :datetime
        DateTime.parse value
      when :boolean
        value ? true : false
      else
        value
      end
    end
  end

  def self.included(base)
    base.extend ClassMethods
    base.attr_reader :attributes
  end
end
# END
