# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    current_locale = I18n.locale.to_s
    new_locale = extract_locale(env)
    Rails.logger.debug "Current locale: #{I18n.locale}"
    if new_locale && current_locale != new_locale
      I18n.locale = new_locale
      Rails.logger.debug "Locale has been changed to #{new_locale}"
    end
    app.call(env)
  end

  private

  def extract_locale(env)
    env.fetch('HTTP_ACCEPT_LANGUAGE', '').scan(/^[a-z]{2}/).first
  end
  # END
end
