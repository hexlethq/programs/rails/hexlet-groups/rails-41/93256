# frozen_string_literal: true

require('digest')

class Signature
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    status, headers, body = @appl.call(env)
    if status == 200
      hash = Digest::SHA2.new(256).hexdigest body
      [200, headers, "#{body}\n#{hash}"]
    else
      [status, headers, body]
    end
  end
end
