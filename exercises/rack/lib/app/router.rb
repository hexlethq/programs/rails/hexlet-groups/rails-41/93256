# frozen_string_literal: true

class Router
  def call(env)
    request = Rack::Request.new(env)
    case request.path
    when '/' || '/admin'
      [200, { 'Content-Type' => 'text/html' }, 'Hello, World!']
    when '/about'
      [200, { 'Content-Type' => 'text/html' }, 'About page']
    else
      [404, {}, '404 Not Found']
    end
  end
end
