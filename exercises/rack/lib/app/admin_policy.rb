# frozen_string_literal: true

class AdminPolicy
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    status, headers, body = @appl.call(env)
    request = Rack::Request.new(env)
    if request.path.start_with? '/admin'
      [403, headers, '']
    else
      [status, headers, body]
    end
  end
end
