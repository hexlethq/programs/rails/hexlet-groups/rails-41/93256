# frozen_string_literal: true

class Timer
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    status, headers, body = @appl.call(env)
    runtime = headers['X-Runtime']
    [status, headers, "#{body}\n#{runtime}"]
  end
end
