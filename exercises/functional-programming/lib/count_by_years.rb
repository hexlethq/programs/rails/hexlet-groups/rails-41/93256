# frozen_string_literal: true

# BEGIN
def count_by_years(users_list)
  result = {}
  male_list = users_list.select { |user| user[:gender] == 'male' }
  male_list.each do |user|
    year = user[:birthday].split('-').first
    if result.key?(year)
      result[year] += 1
    else
      result[year] = 1
    end
  end
  result
end
# END
