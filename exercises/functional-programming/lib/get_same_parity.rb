# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  return list if list.empty?

  first_elem = list.first
  first_elem.odd? ? list.select(&:odd?) : list.select(&:even?)
end
# END
