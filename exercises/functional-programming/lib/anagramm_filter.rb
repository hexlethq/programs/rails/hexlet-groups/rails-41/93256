# frozen_string_literal: true

# BEGIN
def anagramm_filter(test_word, words_list)
  return words_list if words_list.empty?

  words_list.select do |word|
    word.chars.sort == test_word.chars.sort
  end
end
# END
