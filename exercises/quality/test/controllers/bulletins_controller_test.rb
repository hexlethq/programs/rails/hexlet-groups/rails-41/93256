# frozen_string_literal: true

require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  test 'index' do
    get bulletins_path
    assert_response :success
    assert_select 'li', 'Bulletin title 1'
    assert_select 'li', 'Bulletin title 2'
  end

  test 'show' do
    get bulletin_path(1)
    assert_response :success
    assert_select 'p', 'Bulletin body 1'
  end
end
