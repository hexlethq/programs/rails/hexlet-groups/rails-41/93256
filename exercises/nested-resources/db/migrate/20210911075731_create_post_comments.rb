class CreatePostComments < ActiveRecord::Migration[6.1]
  def change
    create_table :post_comments do |t|
      t.text :body
      t.belongs_to :post

      t.timestamps
    end
  end
end
