class Posts::CommentsController < ApplicationController
  before_action :set_post, only: %i[create destroy]
  def create
    if @post.comments.create(comment_params)
      redirect_to post_path(@post), notice: 'Comment created'
    else
      redirect_to post_path(@post)
    end
  end

  def edit
    @comment = PostComment.find(params[:id])
  end

  def update
    @comment = PostComment.find(params[:id])

    if @comment.update(comment_params)
      redirect_to post_path(@comment.post_id)
    else
      render :edit
    end
  end

  def destroy
    comment = PostComment.find(params[:id])
    if comment.destroy
      redirect_to post_path(@post), notice: 'Comment successfully deleted'
    else
      redirect_to post_path(@post), notice: 'Error'
    end
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def comment_params
    params.require(:post_comment).permit(:body)
  end
end
