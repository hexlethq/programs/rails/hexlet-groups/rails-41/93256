namespace :hexlet do
  desc 'Creates users from CSV file'
  task :import_users, [:file_path] => [:environment] do |_t, args|
    path = args[:file_path]
    abort('File path not specified') if path.nil?
    abort('File not found') unless File.file?(path)
    data = File.read(path)
    csv = CSV.parse(data, headers: true)
    print 'Creating users'
    csv.each do |row|
      print '.'
      User.create!(row.to_hash)
    end
    print "\nDone!\n"
  end
end
