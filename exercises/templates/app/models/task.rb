class Task < ApplicationRecord
  validates :name, :status, :creator, presence: { message: "can't be blank" }
  validates :name, :status, :creator, format: { with: /\A[a-zA-Z ]+\z/,
                                                message: 'only allows letters and spaces' }
  validates :completed, inclusion: { in: [true, false], message: 'only allow true or false' }
  validates :completed, exclusion: [nil]
end
