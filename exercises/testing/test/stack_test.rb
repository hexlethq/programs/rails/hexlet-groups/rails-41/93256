# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_to_array
    assert_equal @stack.to_a, []
  end

  def test_empty_stack
    assert @stack.to_a.empty?
    assert @stack.empty?
  end

  def test_push
    @stack.push! 'hello'
    assert_equal @stack.to_a, ['hello']
  end

  def test_pop
    @stack.pop!
    assert_equal @stack.to_a, []
    @stack.push! 'first'
    @stack.push! 'last'
    assert_equal @stack.to_a, %w[first last]
    assert_equal @stack.pop!, 'last'
    assert_equal @stack.to_a, ['first']
  end

  def test_size
    @stack.push! 'hello'
    @stack.push! 'world'
    assert_equal @stack.size, 2
  end

  def test_clear
    @stack.clear!
    assert_equal @stack.to_a, []
    @stack.push! 'one'
    stack_content = @stack.push! 'two'
    assert_equal stack_content, %w[one two]
    @stack.clear!
    assert @stack.to_a, []
  end

  def test_types
    @stack.push! 'hello'
    @stack.push! 10
    @stack.push! nil
    assert_equal @stack.to_a, ['hello', 10, nil]
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
