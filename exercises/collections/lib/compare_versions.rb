# frozen_string_literal: true

# BEGIN
def compare_versions(ver_one, ver_two)
  maj_one, min_one = ver_one.split('.').map(&:to_i)
  maj_two, min_two = ver_two.split('.').map(&:to_i)
  return 1 if (maj_one > maj_two) || (maj_one == maj_two && min_one > min_two)
  return 0 if ver_one == ver_two

  -1
end
# END
