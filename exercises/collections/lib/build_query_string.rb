# frozen_string_literal: true

# rubocop:disable Style/For
# BEGIN
def build_query_string(list)
  list
    .sort
    .each_with_object([]) { |(key, value), query_array| query_array << "#{key}=#{value}" }
    .join('&')
end
# END
# rubocop:enable Style/For
