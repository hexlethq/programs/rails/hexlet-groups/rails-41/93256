# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      page = URI.open('https://rails-l4-collective-blog.herokuapp.com/users/sign_up')
      content = Nokogiri::HTML(page.read)
      cookie = page.meta['set-cookie']
      token = content.search('[name="authenticity_token"]').attribute('value').value
      form_data = {
        email: email,
        password: password,
        password_confirmation: password
      }
      params = {
        user: form_data,
        authenticity_token: token
      }
      request = Net::HTTP::Post.new('/users', Cookie: cookie)
      request.set_form_data(params)
      Net::HTTP.start('rails-l4-collective-blog.herokuapp.com', 443, use_ssl: true) do |http|
        http.request(request)
      end
    end
    # END
  end
end
