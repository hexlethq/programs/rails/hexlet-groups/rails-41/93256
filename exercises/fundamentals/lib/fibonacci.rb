# frozen_string_literal: true

# BEGIN
def fib(num)
  return num if num < 2

  fib(num - 2) + fib(num - 1)
end

def fibonacci(number)
  return nil if number.negative?

  fib(number - 1)
end
# END
