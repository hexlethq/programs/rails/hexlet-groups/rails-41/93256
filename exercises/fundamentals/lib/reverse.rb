# frozen_string_literal: true

# BEGIN
def reverse(string)
  result = ''
  index = string.length - 1
  while index >= 0
    result += string[index]
    index -= 1
  end
  result
end
# END
