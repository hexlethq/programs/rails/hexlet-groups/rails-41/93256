# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  result = []
  count = start
  while count <= stop
    result << if (count % 3).zero? && (count % 5).zero?
                'FizzBuzz'
              elsif (count % 3).zero?
                'Fizz'
              elsif (count % 5).zero?
                'Buzz'
              else
                count
              end
    count += 1
  end
  result.join(' ')
end
# END
