# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  def setup
    @post = posts(:one)
  end
  test 'show index' do
    visit posts_path
    assert_selector 'h1', text: 'Posts'
  end

  test 'show post' do
    visit post_path(@post)
    assert_selector 'h1', text: @post.title
  end

  test 'create post' do
    visit new_post_path
    fill_in 'Title', with: @post.title
    fill_in 'Body', with: @post.body
    click_on 'Create Post'
    assert_text 'Post was successfully created.'
  end

  test 'update post' do
    visit posts_path
    click_on 'Edit', match: :first
    fill_in 'Body', with: @post.body
    click_on 'Update Post'
    assert_text 'Post was successfully updated.'
  end

  test 'delete post' do
    visit posts_path
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end
    assert_text 'Post was successfully destroyed.'
  end

  test 'create comment' do
    visit post_path(@post)
    fill_in 'post_comment[body]', with: @post.comments.first.body
    click_on 'Create Comment'
    assert_text 'Comment was successfully created.'
  end

  test 'update comment' do
    visit post_path(@post)
    click_on 'Edit', match: :first
    fill_in 'Body', with: @post.comments.first.body
    click_on 'Update Comment'
    assert_text 'Comment was successfully updated.'
  end
end
# END
