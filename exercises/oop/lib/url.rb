# frozen_string_literal: true

# BEGIN
require('uri')
require('forwardable')

class Url
  extend Forwardable
  include Comparable

  attr_reader :uri

  def_delegators :@uri, :scheme, :host
  def initialize(url_string)
    @uri = URI(url_string)
  end

  def query_params
    return {} if @uri.query.nil?

    @uri.query.split('&').each_with_object({}) do |param, result|
      key, value = param.split('=')
      result[key.to_sym] = value
    end
  end

  def query_param(key, default = nil)
    query_params[key] || default
  end

  def ==(other)
    @uri == other.uri
  end
end
# END
