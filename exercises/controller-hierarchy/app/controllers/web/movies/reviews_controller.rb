# frozen_string_literal: true

module Web
  module Movies
    class ReviewsController < Web::Movies::ApplicationController
      def index
        @reviews = Review.where(movie_id: params[:movie_id])
      end

      def show
        @reivew = resource_review
      end

      def new
        @review = Review.new
      end

      def edit
        @review = resource_review
      end

      def create
        movie = resource_movie
        if movie.reviews.create(review_params)
          redirect_to movie_reviews_path(movie), notice: t('success')
        else
          render :new, notice: t('fail')
        end
      end

      def update
        if resource_review.update(review_params)
          redirect_to movie_reviews_path(resource_review.movie_id), notice: t('success')
        else
          render :edit, notice: t('fail')
        end
      end

      def destroy
        if resource_review.destroy
          redirect_to movie_path(resource_review.movie_id), notice: t('.success')
        else
          redirect_to movie_path(resource_review.movie_id), notice: t('.fail')
        end
      end

      private

      def resource_review
        @resource_review ||= Review.find params[:id]
      end

      def review_params
        params.require(:review).permit(:body)
      end
    end
  end
end
