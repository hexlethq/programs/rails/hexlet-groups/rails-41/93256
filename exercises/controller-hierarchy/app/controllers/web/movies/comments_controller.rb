# frozen_string_literal: true

module Web
  module Movies
    class CommentsController < Web::Movies::ApplicationController
      def index
        @comments = Comment.where(movie_id: params[:movie_id])
      end

      def show
        @comment = resource_comment
      end

      def new
        @comment = Comment.new
      end

      def edit
        @comment = resource_comment
      end

      def create
        movie = resource_movie
        if movie.comments.create(comment_params)
          redirect_to movie_comments_path(movie), notice: t('success')
        else
          render :new, notice: t('fail')
        end
      end

      def update
        if resource_comment.update(comment_params)
          redirect_to movie_comments_path(resource_comment.movie_id), notice: t('success')
        else
          render :edit, notice: t('fail')
        end
      end

      def destroy
        if resource_comment.destroy
          redirect_to movie_path(resource_comment.movie_id), notice: t('.success')
        else
          redirect_to movie_path(resource_comment.movie_id), notice: t('.fail')
        end
      end

      private

      def resource_comment
        @resource_comment ||= Comment.find params[:id]
      end

      def comment_params
        params.require(:comment).permit(:body)
      end
    end
  end
end
